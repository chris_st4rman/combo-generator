module.exports = {
    mode: 'jit',  // <--- enable JIT
    purge: ['./index.html', './src/**/*.{vue,js,ts,jsx,tsx}'], // you still need purge paths so it knows where to find classes
    darkMode: 'class', // or 'media' or 'class'
    theme: {
      extend: {},
    },
    variants: {
      extend: {},
    },
    plugins: [],
}