<!DOCTYPE html>
<html lang="{{ App::getLocale() }}">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>
        {{ isset($title) ? $title : '' }}
    </title>

    <meta name="description" content="A combo generator, input a context, and get all the possible combos!" />

    @vite(['resources/css/app.css'])

    <script>
        window.Laravel = {!! json_encode(['csrfToken' => csrf_token()]) !!};
    </script>

    @livewireStyles
</head>

<body>
    {{ $body }}

    @include('components/footer')

    @vite(['resources/js/app.js'])

    @livewireScripts
</body>

</html>
